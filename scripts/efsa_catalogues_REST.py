#!/usr/bin/env python3

# %% Packages

# Python Standard Packages
import os
import json
import http.client  # for REST
import urllib.request  # for REST
import urllib.parse  # for REST
import urllib.error  # for REST
import base64  # for REST

# import sys
# import os
# import logging
# import traceback
# import socket
# import re
# import datetime
# import json
# from copy import deepcopy
from pprint import pprint as print

# Other Packages
# import pandas as pd

# %% XML to Dict Parser

import xml.etree.ElementTree as ET

# from: https://stackoverflow.com/questions/2148119/how-to-convert-an-xml-string-to-a-dictionary
class XMLToDictionary(dict):
    def __init__(self, parentElement):
        self.parentElement = parentElement
        for child in list(parentElement):
            child.text = child.text if (child.text != None) else ' '
            if len(child) == 0:
                self.update(self._addToDict(key=child.tag, value=child.text.strip(), dict=self))
            else:
                innerChild = XMLToDictionary(parentElement=child)
                self.update(self._addToDict(key=innerChild.parentElement.tag, value=innerChild, dict=self))

    def getDict(self):
        return {self.parentElement.tag: self}

    class _addToDict(dict):
        def __init__(self, key, value, dict):
            if not key in dict:
                self.update({key: value})
            else:
                identical = dict[key] if type(dict[key]) == list else [dict[key]]
                self.update({key: identical + [value]})


# %% Catalogues REST

# Catalogues (Harmonized controlled terminology) are a key element in the process of data validation and reporting. A harmonized terminology is used to collect and analyse data in a coherent way with the aim to support scientific research.
# This API allows to retrieve the catalogues published on Data Collection Framework.

# import subscription key
api_key_file = os.path.abspath(os.path.join(os.sep, 'home', 'brendy', 'data', 'git', 'beone-metadata-parsing', 'resources', 'efsa_api_key'))
with open(api_key_file, 'r') as file:
    api_key = file.readline().rstrip()

# Define request Headers
headers = {
    # Request headers
    'Content-Type'             : 'application/json',
    'Ocp-Apim-Subscription-Key': api_key,
}

params = urllib.parse.urlencode({

})

domain_url = 'openapi.efsa.europa.eu'
base_url = '/api/catalogues/'


def efsaResponse(body, end_point):
    try:
        conn = http.client.HTTPSConnection(host=domain_url)
        conn.request(method="POST", url=base_url + end_point, body=json.dumps(body), headers=headers)
        response = conn.getresponse()
        data = response.read()
        conn.close()
        return (data)
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))


# %% CatalogueGroupList

# Example from https://openapi-portal.efsa.europa.eu/docs/services/catalogues.REST/operations/5aaf82b5a4972b4859afa52f?
# This operation allows downloading the list of catalogue groups defined in the system. The operation does not have any input parameters. The web service replies with a message containing the list of Groups in XML format.

body = {"getCatalogueGroupList": ""}

data = efsaResponse(body=body, end_point='catalogue-group-list?%s' % params)
data_json = json.loads(data)
xml_str = data_json['getCatalogueGroupListResponse']['return']

parseredDict = XMLToDictionary(ET.fromstring(xml_str)).getDict()
print(parseredDict)

# %% CatalogueFile

# Example from https://openapi-portal.efsa.europa.eu/docs/services/catalogues.REST/operations/5aafa415c0c61299adefd1f4?
# Export functionalities are supported by the method ExportCatalogueFile which differentiates the operation through the exportType parameter. In case that parameter is not provided or mistyped then the web service replies with the fault message: Operation not supported
#
# * Export catalogue
# * Export the release note for a catalogue
# * Export a hierarchy
# * Export a group

body = {
    "ExportCatalogueFile":
        {
            "catalogueCode"   : "GENDER",
            "catalogueVersion": "2.4",
            "exportType"      : "catalogFullDefinition",
            "group"           : "",
            "dcCode"          : "",
            "fileType"        : "XML"
        }
}

data = efsaResponse(body=body, end_point='catalogue-file?%s' % params)

attachment = data.decode(encoding='UTF-8').split('\r\n\r\n', 1)[1]
attachment2 = attachment.split('\r\n\r\n', 1)[1]
xml_str = attachment2.split('\n\r\n', 1)[0]

parseredDict = XMLToDictionary(ET.fromstring(xml_str)).getDict()
print(parseredDict)

print(parseredDict['message']['catalogue']['catalogueTerms']['term'])

# %% CatalogueList

# This operation allows downloading the list of catalogues, possibly restricted to a certain catalogue group or data collection.
#
# * Download catalogues for a data collection
# * Download catalogues in a group

body = {
    "getCatalogueList":
        {
            "arg0": "Geographical distribution",
            "arg1": "",
            "arg2": "XML"
        }
}

data = efsaResponse(body=body, end_point='catalogue-list?%s' % params)

data_json = json.loads(data)
xml_str = ET.fromstring(data_json['getCatalogueListResponse']['return'])

parseredDict = XMLToDictionary(xml_str).getDict()
print(parseredDict)
