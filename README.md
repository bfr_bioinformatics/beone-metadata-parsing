# BeONE Metadata Parsing

The [Metadata Submission Form](form/BeONE_template_WGS_and_metadata_submission.xlsx) allows users to select terms from a controlled vocabulary without the need for complex Microsoft Excel macros.

# Controlled Vocabulary
The hierarchical terms are derived from the [EFSA Data Collection Framework](https://zenodo.org/record/4495166).
Review the Zenodo files `Catalogue_list.xlsx` for a description of the files in `DCF_catalogues.zip`.
Copy the downloaded files in the `resources` directory with the name of the data source, e.g. `/resources/zenodo.org_record_4495166/DCF_catalogues/*.xlsx`

### Generate Excel Form
To implement the DCF catalogs in the Metadata Submission Form, a data transformation is performed with the R script [efsa_dcf_catalogue_parser.R](scripts/efsa_dcf_catalogue_parser.R).

### REST query the DCF catalogue from the EFSA OpenAPI server
Request a REST API [key](resources/efsa_api_key) from [EFSA](https://openapi-portal.efsa.europa.eu/signin) and test [efsa_catalogues_REST.py](scripts/efsa_catalogues_REST.py).

### Read Excel Form into R
To parse sample information into a JSON structure, the R script [beone_metadata_parser.R](scripts/beone_metadata_parser.R) reads and translates sample information.

## Troubleshooting

If Rscript executions stops with the error 
```
/usr/lib/x86_64-linux-gnu/libstdc++.so.6: version `GLIBCXX_3.4.30' not found (required by /home/brendy/miniconda3/envs/beone-metadata-parser/lib/R/library/vroom/libs/vroom.so)
```
, then the conda environment library path is not utilized.
Run `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${CONDA_PREFIX}/lib` _after_ `conda activate beone-metadata-parsing`.
